/*
 * ░▀█▀░█░█░▀█▀░█▀▀░░░█▀▀░█▀█░█▀▄░█▀▀░░░▀█▀░█▀▀░░░█▀▄░█▀█░█▀█░█▀█░█▀▄
 * ░░█░░█▀█░░█░░▀▀█░░░█░░░█░█░█░█░█▀▀░░░░█░░▀▀█░░░█▀▄░█▀█░█▀█░█▀█░█░█
 * ░░▀░░▀░▀░▀▀▀░▀▀▀░░░▀▀▀░▀▀▀░▀▀░░▀▀▀░░░▀▀▀░▀▀▀░░░▀▀░░▀░▀░▀░▀░▀░▀░▀▀░
 * ░█▀▄░█▀█░█▀█░▀█▀░░░█▀▀░█░█░█▀▀░█▀█░░░█▀▄░█▀█░▀█▀░█░█░█▀▀░█▀▄
 * ░█░█░█░█░█░█░░█░░░░█▀▀░▀▄▀░█▀▀░█░█░░░█▀▄░█░█░░█░░█▀█░█▀▀░█▀▄
 * ░▀▀░░▀▀▀░▀░▀░░▀░░░░▀▀▀░░▀░░▀▀▀░▀░▀░░░▀▀░░▀▀▀░░▀░░▀░▀░▀▀▀░▀░▀
 * */


var width = window.innerWidth
var height = window.innerHeight

const circleSpacing = 80


function setup() {
  createCanvas(windowWidth, windowHeight)
  colorMode(HSB, 255)
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
  width = windowWidth
  height = windowHeight
}

function euclid(p1, p2) {
  return sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)
}

var msense = 1/300
function mouseWheel(event) {
  console.log(event.delta);
  msense += event.delta / 200000;
}

function circleField(targetX, targetY) {
  stroke(255, 128, 180)
  noFill()
  for (var y = 0; y <= height; y += circleSpacing) {
    for (var x = 0; x <= width; x += circleSpacing) {
      var wantRadius = circleSpacing - (50 * cos(msense * euclid([x, y], [targetX, targetY])))
      var realRadius = max([wantRadius, 1])
      realRadius = min([realRadius, circleSpacing * sqrt(2)])

      circle(x, y, realRadius)
    }
  }
}


// swimmer state
var cX = width / 2
var cY = height / 2
var cXn = 1354135
var cYn = 123

function swimmer() {
  fill(255, 128, 200)
  stroke(255, 128, 200)
  dx = 2 * (noise(cXn) - 0.5)
  dy = 2 * (noise(cYn) - 0.5)

  cX += dx
  cY += dy
  cXn += 0.001
  cYn += 0.001

  circle(cX, cY, 10)
  circle(cX - 10*dx, cY - 10*dy, 9)
  circle(cX - 20*dx, cY - 20*dy, 8)
  circle(cX - 30*dx, cY - 30*dy, 6)
  circle(cX - 40*dx, cY - 40*dy, 4)
  circle(cX - 50*dx, cY - 50*dy, 2)
}

function draw() {
  const bgcolor = color(200, 128, 20)
  background(bgcolor)

  circleField(cX, cY)
  swimmer()
}
